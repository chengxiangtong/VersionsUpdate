package www.versionsupdate.com.versionsupdate.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import www.versionsupdate.com.versionsupdate.MainActivity;
import www.versionsupdate.com.versionsupdate.R;
import www.versionsupdate.com.versionsupdate.UpdateManage;
import www.versionsupdate.com.versionsupdate.utils.AppUtils;

/**
 * Created by Administrator on 2016/6/29 0029.
 */
public class SplashActivity extends Activity {

    private Context context;
    private int versionCode;

    private boolean isDown=false;

    private final int GO_HOME=100;
    private final int SHOW_DIALOG=101;
    private String updateName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context=this;

        //获得当前版本号
        versionCode = AppUtils.getVersionCode(this);

        //假设我们在子线程联网后获取服务器版本号和版本名称(真是项目中都是从服务器获取的)
        int updateCode = 3;
        updateName = "更新免费送英雄，皮肤活动！";


        //然后判断服务器版本号是否和当前版本号一样
        if (updateCode == versionCode) {//发送一个handler进入主页面
            handler.sendEmptyMessage(GO_HOME);
        } else {//弹出对话框提示更新
            handler.sendEmptyMessage(SHOW_DIALOG);
        }

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case GO_HOME:
                    //去主页面
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                    break;

                case SHOW_DIALOG:
                    showUpdateDialog();
                    break;
            }
        }
    };

    private ProgressDialog pd;

    private void showUpdateDialog() {
        //弹出对话框提示更新
        AlertDialog.Builder adb = new AlertDialog.Builder(SplashActivity.this);
        adb.setTitle("发现新版本");
        adb.setMessage(updateName);

        adb.setCancelable(false);//要么点击确定，要么点击取消。否则不会关闭dialog

        adb.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //下载更新的APK
                downUpdateAPK();
            }
        });
        adb.setPositiveButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //点击取消进入主页面
                handler.sendEmptyMessage(GO_HOME);
            }
        });
        adb.show();
    }

    /**
     * 下载新的APK
     */
    private void downUpdateAPK() {
        UpdateManage updateManage = new UpdateManage(SplashActivity.this, "http://192.168.1.98/app-debug.apk");
        updateManage.showDownloadDialog();//http://192.168.1.98/app-debug.apk
        isDown=true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isDown==true){
            handler.sendEmptyMessageDelayed(GO_HOME,2500);
        }
    }
}
